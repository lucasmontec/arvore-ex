﻿using System;
using System.Collections.Generic;
using System.Linq;
using _SnakeGame.SnakeFeature;
using _SnakeGame.SnakeFeature.InputControllers;
using _SnakeGame.Util;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _SnakeGame.MenuFeature.Source
{
    public class PlayerPanelView : SerializedMonoBehaviour
    {
        public Sprite humanPlayerIcon;
        public Sprite aiPlayerIcon;

        [Required]
        public SnakeType[] initialSnakeTypes;
        
        [Required]
        public Image playerTypeIcon;
        [Required]
        public TextMeshProUGUI panelTitle;
        [Required]
        public TextMeshProUGUI controlsLabel;
        
        [Required]
        public GameObject instructionsPanelRoot;
        [Required]
        public TextMeshProUGUI instructionsLabel;

        [Required]
        public Image holdKeysTimer;
        
        [Required]
        public GameObject snakeSelectPanelRoot;
        [Required]
        public SnakePreviewView snakePreview;
        [Required]
        public TextMeshProUGUI snakeSelectKeyLeft;
        [Required]
        public TextMeshProUGUI snakeSelectKeyRight;

        public event Action OnControlsSelected;
        
        private SnakeController _snakeController = SnakeController.AI;
        
        private int _selectedSnakeType = 0;

        private bool _startedSelecting;
        private float _selectionStartTime;

        private KeyCode _leftKey, _rightKey;

        private bool _choosingSnakeType;

        private static HashSet<KeyCode> _usedKeys = new();

        public SnakeConfiguration GetSnakeConfiguration =>
            new()
            {
                controller = _snakeController,
                aiDifficulty = SnakeAIInputController.AIDifficulty.Hard,
                input = new PlayerInputKeys(_leftKey, _rightKey),
                snakeType = initialSnakeTypes[_selectedSnakeType]
            };

        public void SetTitleNoPlayer()
        {
            panelTitle.text = $"Player {transform.GetSiblingIndex() + 1} - none";
            playerTypeIcon.sprite = aiPlayerIcon;
        }
        
        public void SetTitleAiPlayer()
        {
            panelTitle.text = $"Player {transform.GetSiblingIndex() + 1} - AI";
            instructionsLabel.text = "This player is an AI. Press and hold two keys to play in this slot and select your snake.";
            playerTypeIcon.sprite = aiPlayerIcon;
        }
        
        private void SetTitleHumanPlayer()
        {
            panelTitle.text = $"Player {transform.GetSiblingIndex() + 1}";
            playerTypeIcon.sprite = humanPlayerIcon;
        }

        public void ShowInstructions()
        {
            snakeSelectPanelRoot.SetActive(false);
            instructionsPanelRoot.SetActive(true);
            
            controlsLabel.text = "<Select controls>";
        }

        private void Update()
        {
            HandleControlsSelection();
            ShowSelectionTimer();

            HandleChooseSnakeType();
        }

        private void HandleChooseSnakeType()
        {
            if(!_choosingSnakeType) return;
            if (Input.GetKeyDown(_leftKey))
            {
                _selectedSnakeType--;
                if (_selectedSnakeType < 0)
                {
                    _selectedSnakeType = initialSnakeTypes.Length - 1;
                }
                snakePreview.ShowType(initialSnakeTypes[_selectedSnakeType]);
            }
            
            if (Input.GetKeyDown(_rightKey))
            {
                _selectedSnakeType++;
                if (_selectedSnakeType >= initialSnakeTypes.Length)
                {
                    _selectedSnakeType = 0;
                }
                snakePreview.ShowType(initialSnakeTypes[_selectedSnakeType]);
            }
        }

        private void ShowSelectionTimer()
        {
            if (!_startedSelecting) return;
            holdKeysTimer.fillAmount = Mathf.Clamp01(Time.time - _selectionStartTime);
        }

        private void HandleControlsSelection()
        {
            if(_choosingSnakeType) return;
            List<KeyCode> pressedKeys = KeyInputUtil.GetPressedKeys().Where(key => !_usedKeys.Contains(key)).ToList();
            switch (pressedKeys.Count)
            {
                case 2 when !_startedSelecting:
                    StartSelectingControls();
                    break;
                case < 2:
                    StopSelectingControls();
                    instructionsPanelRoot.SetActive(true);
                    return;
            }

            if (pressedKeys.Count == 2 && Time.time - _selectionStartTime >= 1)
            {
                StopSelectingControls();
                _leftKey = pressedKeys[0];
                _rightKey = pressedKeys[1];

                _snakeController = SnakeController.Human;
                SetTitleHumanPlayer();
                SetupSnakePreview();

                _usedKeys.Add(_leftKey);
                _usedKeys.Add(_rightKey);
                OnControlsSelected?.Invoke();
            }
        }

        private void SetupSnakePreview()
        {
            _choosingSnakeType = true;
            snakeSelectPanelRoot.SetActive(true);
            snakePreview.ShowType(initialSnakeTypes[_selectedSnakeType]);
            snakeSelectKeyLeft.text = _leftKey.ToString();
            snakeSelectKeyRight.text = _rightKey.ToString();
            controlsLabel.text = $"Controls: {_leftKey.ToString()} {_rightKey.ToString()}";
        }

        private void StopSelectingControls()
        {
            holdKeysTimer.gameObject.SetActive(false);
            _startedSelecting = false;
        }
        
        private void StartSelectingControls()
        {
            instructionsPanelRoot.SetActive(false);
            holdKeysTimer.gameObject.SetActive(true);
            _startedSelecting = true;
            _selectionStartTime = Time.time;
        }
    }
}