﻿using _SnakeGame.SnakeFeature;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace _SnakeGame.MenuFeature.Source
{
    public class SnakePreviewView : SerializedMonoBehaviour
    {
        public Transform previewRoot;

        [Button]
        public void ShowType(SnakeType type)
        {
            ClearChildren();
            SpawnBlockViews(type);
        }

        private void SpawnBlockViews(SnakeType type)
        {
            foreach (var block in type.Blocks)
            {
                var snakePartPreview = new GameObject("Part Preview");
                var rectTransform = snakePartPreview.AddComponent<RectTransform>();
                rectTransform.SetParent(previewRoot, false);

                var partPreviewImage = new GameObject("Part Preview Non Layouted");
                rectTransform = partPreviewImage.AddComponent<RectTransform>();
                rectTransform.SetParent(snakePartPreview.transform, false);
                
                var imageComponent = partPreviewImage.AddComponent<Image>();
                imageComponent.transform.rotation = Quaternion.Euler(0,0,-90);
                imageComponent.sprite = block.sprite;
            }
        }

        private void ClearChildren()
        {
            foreach (Transform child in previewRoot)
            {
                Destroy(child.gameObject);
            }
        }
    }
}