﻿using System.Collections.Generic;
using System.Linq;
using _SnakeGame.GameFeature;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _SnakeGame.MenuFeature.Source
{
    public class MainMenuPlayerSelectorView : SerializedMonoBehaviour
    {
        [Required, AssetsOnly]
        public PlayerPanelView playerPanelViewPrefab;

        [Required]
        public TextMeshProUGUI playInstructions;
        
        [Required]
        public GameSettings gameSettings;
        
        private readonly List<PlayerPanelView> _playerPanels = new();

        private void Start()
        {
            CreateFirstPlayerPanel();
        }

        private void CreateFirstPlayerPanel()
        {
            var panel = Instantiate(playerPanelViewPrefab, transform);
            panel.SetTitleNoPlayer();
            panel.ShowInstructions();
            
            _playerPanels.Add(panel);
            panel.OnControlsSelected += CreatePlayerPanel;
        }
        
        private void CreatePlayerPanel()
        {
            var panel = Instantiate(playerPanelViewPrefab, transform);
            panel.SetTitleAiPlayer();
            panel.ShowInstructions();
            
            _playerPanels.Add(panel);
            panel.OnControlsSelected += CreatePlayerPanel;
            
            playInstructions.gameObject.SetActive(true);
        }

        private void Update()
        {
            HandleStartGame();
        }

        private void HandleStartGame()
        {
            if (!Input.GetKeyDown(KeyCode.Return)) return;
            if (_playerPanels.Count < 2) return;
            gameSettings.snakes = _playerPanels.Select(panel => panel.GetSnakeConfiguration).ToArray();
            SceneManager.LoadScene(1);
        }
    }
}