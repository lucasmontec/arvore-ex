﻿namespace _SnakeGame.Framework
{
    public interface IUpdatable
    {
        void Update();
    }
}