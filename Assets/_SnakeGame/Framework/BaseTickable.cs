﻿using UnityEngine;

namespace _SnakeGame.Framework
{
    public abstract class BaseTickable : IUpdatable
    {
        private float _tickInterval;
        private float _nextTick;

        protected BaseTickable(float tickInterval)
        {
            _tickInterval = tickInterval;
        }

        public void Update()
        {
            if (Time.time < _nextTick) return;
            _nextTick = Time.time + _tickInterval;
            
            Tick();
        }

        protected void ChangeTickInterval(float newInterval)
        {
            _tickInterval = newInterval;
        }

        protected abstract void Tick();
    }
}