﻿using System.Collections.Generic;
using _SnakeGame.BlockFeature;
using _SnakeGame.SnakeFeature;
using Unity.Mathematics;

namespace _SnakeGame.SaveFeature
{
    public class SnakeSave
    {
        public SnakeConfiguration SnakeConfiguration;
        public IReadOnlyList<int2> SnakePartPositions;
        public IReadOnlyList<BaseBlock> SnakeBlocks;

        public static SnakeSave CreateFrom(Snake snake)
        {
            SnakeSave save = new()
            {
                SnakePartPositions = snake.PartPositions.AsReadOnly(),
                SnakeBlocks = snake.PartBlocks.AsReadOnly(),
                SnakeConfiguration = snake.Configuration.Clone()
            };
            return save;
        }
    }
}