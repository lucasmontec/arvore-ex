﻿using System.Collections.Generic;
using System.Linq;
using _SnakeGame.BlockFeature.Pickup;
using _SnakeGame.MapFeature;
using Unity.Mathematics;

namespace _SnakeGame.SaveFeature
{
    public class PickupsSave
    {
        public Dictionary<int2, IPickup> Pickups;
        
        public static PickupsSave CreateFrom(Map map)
        {
            var pickups = new Dictionary<int2, IPickup>(
                map.PickupControllers.Where(kvp => kvp.Value.Pickup.IsSavable)
                    .Select(kvp => new KeyValuePair<int2,IPickup>(kvp.Key, kvp.Value.Pickup)));
            
            PickupsSave save = new()
            {
                Pickups = pickups
            };
            return save;
        }
    }
}