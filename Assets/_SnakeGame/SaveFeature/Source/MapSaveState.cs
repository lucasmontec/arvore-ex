﻿using System.Linq;
using _SnakeGame.BlockFeature.Pickup;
using _SnakeGame.GameFeature;
using _SnakeGame.MapFeature;
using _SnakeGame.SnakeFeature;
using Unity.Mathematics;

namespace _SnakeGame.SaveFeature
{
    public class MapSaveState
    {
        private readonly Map _map;
        private readonly PickupFactory _pickupFactory;
        private readonly SnakeFactory _snakeFactory;
        private readonly GameController _gameController;

        private SaveState _stateToLoad;
        
        public MapSaveState(Map map, PickupFactory pickupFactory, SnakeFactory snakeFactory, GameController gameController)
        {
            _map = map;
            _pickupFactory = pickupFactory;
            _snakeFactory = snakeFactory;
            _gameController = gameController;

            gameController.AfterUpdate += LoadPendingState;
        }

        ~MapSaveState()
        {
            _gameController.AfterUpdate -= LoadPendingState;
        }

        private void LoadPendingState()
        {
            if(_stateToLoad == null) return;
            CreatePickups(_stateToLoad);
            CreateSnakes(_stateToLoad);
            _stateToLoad = null;
        }

        public SaveState SaveState()
        {
            return new SaveState
            {
                PickupsSave = PickupsSave.CreateFrom(_map),
                SnakeSaves = _map.Snakes.Select(SnakeSave.CreateFrom).ToList()
            };
        }

        public void LoadState(SaveState state)
        {
            _map.Clear();
            _stateToLoad = state;
        }

        private void CreateSnakes(SaveState state)
        {
            foreach (SnakeSave snakeSave in state.SnakeSaves)
            {
                _snakeFactory.CreateFromSave(snakeSave);
            }
        }

        private void CreatePickups(SaveState state)
        {
            if (state.PickupsSave.Pickups.Count > 0)
            {
                foreach ((int2 position, IPickup pickup) in state.PickupsSave.Pickups)
                {
                    _pickupFactory.CreateAt(pickup, position);
                }
            }
            else
            {
                _pickupFactory.CreateRandomAtRandomPosition();
            }
        }
    }
}