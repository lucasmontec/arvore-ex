﻿using _SnakeGame.BlockFeature.Pickup;
using _SnakeGame.GameFeature;

namespace _SnakeGame.SaveFeature
{
    public class PickupSaveStateDependencySetup : IPickupDependencySetup
    {
        private readonly MapSaveState _mapSaveState;

        public PickupSaveStateDependencySetup(MapSaveState mapSaveState)
        {
            _mapSaveState = mapSaveState;
        }

        public void Setup(IPickup pickup)
        {
            if (pickup is ISaveStatePickup saveStatePickup)
            {
                saveStatePickup.Setup(_mapSaveState);
            }
        }
    }
}