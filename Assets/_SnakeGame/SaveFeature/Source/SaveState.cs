﻿using System.Collections.Generic;

namespace _SnakeGame.SaveFeature
{
    public class SaveState
    {
        public PickupsSave PickupsSave;
        public List<SnakeSave> SnakeSaves;
    }
}