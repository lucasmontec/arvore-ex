﻿using System;
using System.Collections.Generic;
using _SnakeGame.BlockFeature.Pickup;
using _SnakeGame.Framework;
using _SnakeGame.MapFeature;
using _SnakeGame.SaveFeature;
using _SnakeGame.SnakeFeature;
using _SnakeGame.SnakeFeature.Parts;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace _SnakeGame.GameFeature
{
    public class GameController : SerializedMonoBehaviour, IUpdatableManager
    {
        [Required]
        public MapController mapController;
        [Required]
        public GameSettings gameSettings;
        [Required]
        public SnakePart snakePartPrefab;
        [Required]
        public SnakeLineRenderer snakeLineRendererPrefab;

        [Required]
        public Transform snakesRoot;
        
        [Required]
        public PickupController pickupControllerPrefab;
        [Required, OdinSerialize, NonSerialized]
        public IPickup[] Pickups;

        private readonly List<IUpdatable> _updatables = new();
        private readonly Queue<IUpdatable> _removeUpdatable = new();
        
        private readonly Queue<SnakeConfiguration> _snakesToCreate = new();

        private PickupFactory _pickupFactory;
        private SnakeFactory _snakeFactory;

        private MapSaveState _mapSaveState;
        private SaveState _state;

        public event Action AfterUpdate;
        
        private void Start()
        {
            _snakeFactory = new SnakeFactory(new PrefabSnakePartFactory(snakePartPrefab, snakesRoot), mapController.Map,
                gameSettings, this, _snakesToCreate.Enqueue, snakeLineRendererPrefab);
            
            _updatables.Clear();
            CreateSnakes();
            
            _pickupFactory = new PickupFactory(mapController.Map, pickupControllerPrefab, Pickups);
            _mapSaveState = new MapSaveState(mapController.Map, _pickupFactory, _snakeFactory, this);
            
            _pickupFactory.AddDependencySetup(new PickupSaveStateDependencySetup(_mapSaveState));
            _pickupFactory.CreateRandomAtRandomPosition();
        }

        private void CreateSnakes()
        {
            foreach (SnakeConfiguration snakeConfiguration in gameSettings.snakes)
            {
                _snakeFactory.Create(snakeConfiguration);
            }
        }

        public void AddUpdatables(params IUpdatable[] updatables)
        {
            _updatables.AddRange(updatables);
        }

        public void RemoveUpdatables(params IUpdatable[] updatables)
        {
            foreach (IUpdatable updatable in updatables)
            {
                _removeUpdatable.Enqueue(updatable);
            }
        }

        public void Update()
        {
            RemoveUpdatables();
            
            foreach (IUpdatable updatable in _updatables)
            { 
                updatable.Update();
            }

            RespawnPendingSnakes();
        }

        private void LateUpdate()
        {
            AfterUpdate?.Invoke();
        }

        private void RemoveUpdatables()
        {
            while (_removeUpdatable.TryDequeue(out IUpdatable updatable))
            {
                _updatables.Remove(updatable);
            }
        }

        private void RespawnPendingSnakes()
        {
            while (_snakesToCreate.TryDequeue(out SnakeConfiguration configuration))
            {
                _snakeFactory.Create(configuration);
            }
        }
    }
}