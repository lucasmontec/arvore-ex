﻿using System;
using System.Collections.Generic;
using _SnakeGame.BlockFeature.Pickup;
using _SnakeGame.MapFeature;
using _SnakeGame.Util;
using Unity.Mathematics;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = Unity.Mathematics.Random;

namespace _SnakeGame.GameFeature
{
    public class PickupFactory
    {
        private readonly Map _map;
        private readonly PickupController _pickupPrefab;
        private readonly IPickup[] _pickups;
        private Random _random = new((uint)DateTime.Now.Millisecond);
        private readonly HashSet<IPickupDependencySetup> _pickupDependencySetups = new();

        public PickupFactory(Map map, PickupController pickupPrefab, IPickup[] pickups)
        {
            _map = map;
            _pickupPrefab = pickupPrefab;
            _pickups = pickups;
        }

        public void AddDependencySetup(IPickupDependencySetup dependencySetup)
        {
            _pickupDependencySetups.Add(dependencySetup);
        }
        
        public void CreateRandomAtRandomPosition()
        {
            bool spawned;
            do
            {
                if (_map.PickupCount > 0)
                {
                    return;
                }
                
                int2 randomPos = _random.NextInt2(int2.zero, _map.Size);
                spawned = _map.InBounds(randomPos) && _map.IsPositionFree(randomPos);
                var pickup = _pickups[_random.NextInt(0, _pickups.Length)];
                CreateAt(pickup, randomPos);
            } while (!spawned);
        }

        public void CreateAt(IPickup pickup, int2 position)
        {
            var pickupController = Object.Instantiate(_pickupPrefab, position.ToVector3(), Quaternion.identity);
            SetupPickupDependencies(pickup);
            pickupController.Setup(pickup, _pickupPrefab);
            pickupController.OnPickedUp += () =>
            {
                _map.RemovePickup(position);
                CreateRandomAtRandomPosition();
            };
            _map.AddPickup(position, pickupController);
        }

        private void SetupPickupDependencies(IPickup pickup)
        {
            foreach (IPickupDependencySetup dependencySetup in _pickupDependencySetups)
            {
                dependencySetup.Setup(pickup);
            }
        }
    }
}