﻿using _SnakeGame.BlockFeature;
using _SnakeGame.BlockFeature.Pickup;

namespace _SnakeGame.GameFeature
{
    public interface IPickupDependencySetup
    {
        public void Setup(IPickup pickup);
    }
}