﻿using _SnakeGame.Framework;

namespace _SnakeGame.GameFeature
{
    public interface IUpdatableManager
    {
        void AddUpdatables(params IUpdatable[] updatables);
        void RemoveUpdatables(params IUpdatable[] updatables);
    }
}