﻿using System;
using _SnakeGame.SnakeFeature;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _SnakeGame.GameFeature
{
    [CreateAssetMenu(menuName = "Snake/Game Setup")]
    public class GameSettings : SerializedScriptableObject
    {
        public SnakeConfiguration[] snakes = Array.Empty<SnakeConfiguration>();
        public float baseSnakeSpeed = 1;
    }
}