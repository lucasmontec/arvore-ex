﻿using Unity.Mathematics;
using UnityEngine;

namespace _SnakeGame.Util
{
    public static class MathUtil
    {
        public static Vector2 ToVector2(this int2 value)
        {
            return new Vector2(value.x, value.y);
        }

        public static Vector3 ToVector3(this int2 value)
        {
            return new Vector3(value.x, value.y, 0);
        }
        
        public static int2 RotateLeft(this int2 vec) 
        {
            return new int2(-vec.y, vec.x);
        }

        public static int2 RotateRight(this int2 vec) 
        {
            return new int2(vec.y, -vec.x);
        }
    }
}