﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _SnakeGame.Util
{
    public class RandomWeightedList<T>
    {
        private readonly Dictionary<T, float> _objects = new();
        
        /// <summary>
        /// Internal list to cache and fix the selection probability
        /// </summary>
        private readonly List<WeightPair> _backingList = new();
        
        private readonly struct WeightPair
        {
            public readonly T Value;
            public readonly float Weight;

            public WeightPair(T value, float weight)
            {
                Value = value;
                Weight = weight;
            }

            public void Deconstruct(out T value, out float weight)
            {
                value = Value;
                weight = Weight;
            }
        }
        
        private float _emptyReturnChance;
        private readonly bool _inverted;

        private readonly Func<float, float, float> _randomFunc;
        private readonly Func<int, int, int> _intRandomFunc;

        private float _minWeight = float.PositiveInfinity;
        
        public RandomWeightedList(Func<float, float, float> randomFunc, Func<int, int, int> intRandomFunc, float emptyReturnChance=-1f, bool inverted=false)
        {
            _emptyReturnChance = emptyReturnChance;
            _inverted = inverted;
            _randomFunc = randomFunc;
            _intRandomFunc = intRandomFunc;
        }

        public void SetEmptyChance(float emptyChance)
        {
            _emptyReturnChance = emptyChance;
        }
        
        public void Add(T obj, float weight)
        {
            _objects[obj] = weight;
            
            if (weight < _minWeight)
            {
                _minWeight = weight;
            }
        }
        
        /// <summary>
        /// Returns a random object with probability based on its weight.
        /// </summary>
        /// <returns></returns>
        public T GetWeightedRandom() {
            if (_objects == null || !_objects.Any()) 
                return default;

            PrepareBackingListWithElements();

            HandleEmptyEntry();

            SortBackingListByChance();

            float totalChance = _backingList.Select(pair => pair.Weight).Sum()-float.Epsilon;
            float chanceElement = _randomFunc(0f, totalChance);
            
            foreach((T value, float weight) in _backingList) {
                if (chanceElement < weight)
                    return value;
                chanceElement -= weight;
            }

            return _backingList[_intRandomFunc(0, _backingList.Count-1)].Value;
        }

        private void SortBackingListByChance()
        {
            _backingList.Sort((x, y) => x.Weight.CompareTo(y.Weight));
        }

        private void PrepareBackingListWithElements()
        {
            _backingList.Clear();
            foreach ((T obj, float weight) in _objects)
            {
                var finalWeight = weight;
                if (_inverted)
                {
                    finalWeight = 1 / weight;
                }

                finalWeight = NormalizeWeightAboveZero(finalWeight);
                
                _backingList.Add(new WeightPair(obj, finalWeight));
            }
        }

        private float NormalizeWeightAboveZero(float weight)
        {
            if (_minWeight < 0)
            {
                return weight + Mathf.Abs(_minWeight);
            }

            return weight;
        }

        private void HandleEmptyEntry()
        {
            if (_emptyReturnChance <= 0) return;
            _backingList.Add(new WeightPair(default, _emptyReturnChance));
        }
    }
}
