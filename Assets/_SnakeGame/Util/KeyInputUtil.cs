﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _SnakeGame.Util
{
    public static class KeyInputUtil
    {
        private static readonly KeyCode[] keyCodes =
            System.Enum.GetValues(typeof(KeyCode))
                .Cast<KeyCode>()
                .Where(k => k < KeyCode.Mouse0)
                .ToArray();
        
        public static IEnumerable<KeyCode> GetPressedKeys()
        {
            if (!Input.anyKey) yield break;
            
            foreach (KeyCode key in keyCodes)
                if (Input.GetKey(key))
                    yield return key;
        }
    }
}