﻿using _SnakeGame.SnakeFeature;

namespace _SnakeGame.BlockFeature.Pickup
{
    public interface IPrePickupHandler
    {
        void WillPickup(Snake snake);
    }
}