﻿using _SnakeGame.SnakeFeature;
using UnityEngine;

namespace _SnakeGame.BlockFeature.Pickup
{
    public interface IPickup
    {
        void Pickup(Snake snake);
        Sprite Sprite { get; }
        bool IsSavable { get; }
    }
}