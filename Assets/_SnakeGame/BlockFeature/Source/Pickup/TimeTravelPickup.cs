﻿using _SnakeGame.SaveFeature;
using _SnakeGame.SnakeFeature;
using UnityEngine;

namespace _SnakeGame.BlockFeature.Pickup
{
    [CreateAssetMenu(menuName = "Pickups/Time Travel")]
    public class TimeTravelPickup : BaseBlockPickup<TimeTravelBlock>, IPrePickupHandler, ISaveStatePickup
    {
        private SaveState _saveState;
        private MapSaveState _mapSaveState;

        public void Setup(MapSaveState mapSaveState)
        {
            _mapSaveState = mapSaveState;
        }
        
        public void WillPickup(Snake snake)
        {
            _saveState = _mapSaveState.SaveState();
        }

        protected override TimeTravelBlock GetBlock()
        {
            var block = base.GetBlock();
            block.Initialize(_mapSaveState, _saveState);
            return block;
        }
    }
}