﻿using UnityEngine;

namespace _SnakeGame.BlockFeature.Pickup
{
    [CreateAssetMenu(menuName = "Pickups/Block")]
    public class BlockPickup : BaseBlockPickup<Block>
    {
    }
}