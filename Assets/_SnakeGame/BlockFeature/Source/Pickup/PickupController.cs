﻿using System;
using _SnakeGame.SnakeFeature;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _SnakeGame.BlockFeature.Pickup
{
    public class PickupController : SerializedMonoBehaviour
    {
        public event Action OnPickedUp;
        public PickupController Prefab { get; private set; }

        [Required]
        public SpriteRenderer spriteRenderer;
        
        public IPickup Pickup { get; private set; }

        private void UpdatePickupSprite()
        {
            spriteRenderer.sprite = Pickup.Sprite;
        }
        
        public void Setup(IPickup pickup, PickupController prefab)
        {
            Prefab = prefab;
            Pickup = pickup;
            UpdatePickupSprite();
        }
        
        public void DoPickup(Snake snake)
        {
            Pickup.Pickup(snake);
            PickedUp();
        }

        private void PickedUp()
        {
            OnPickedUp?.Invoke();
            Destroy(gameObject);
        }
    }
}