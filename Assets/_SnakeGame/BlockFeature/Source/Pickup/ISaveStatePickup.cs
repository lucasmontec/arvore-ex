﻿using _SnakeGame.SaveFeature;

namespace _SnakeGame.BlockFeature.Pickup
{
    public interface ISaveStatePickup
    {
        public void Setup(MapSaveState mapSaveState);
    }
}