﻿using _SnakeGame.SnakeFeature;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _SnakeGame.BlockFeature.Pickup
{
    public class BaseBlockPickup<TBlock> : ScriptablePickup where TBlock : BaseBlock
    {
        [Required, SerializeField]
        private TBlock block;

        protected virtual TBlock GetBlock()
        {
            return Instantiate(block);
        }
        
        public override void Pickup(Snake snake)
        {
            snake.AddPart(GetBlock());
        }

        public override Sprite Sprite => block.sprite;
    }
}