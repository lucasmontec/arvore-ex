﻿using _SnakeGame.SnakeFeature;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _SnakeGame.BlockFeature.Pickup
{
    public abstract class ScriptablePickup : SerializedScriptableObject, IPickup
    {
        public bool savable;
        
        public abstract void Pickup(Snake snake);
        public abstract Sprite Sprite { get; }
        public bool IsSavable => savable;
    }
}