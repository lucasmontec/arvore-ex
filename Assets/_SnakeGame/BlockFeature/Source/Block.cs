﻿using _SnakeGame.BlockFeature.Collision;
using UnityEngine;

namespace _SnakeGame.BlockFeature
{
    [CreateAssetMenu(menuName = "Snake/Block")]
    public class Block : BaseBlock
    {
        [SerializeField]
        private AbstractCollisionResponse collisionResponse;

        public override ICollisionResponse CollisionResponse => collisionResponse == null ? null : Instantiate(collisionResponse);
    }
}