﻿using _SnakeGame.BlockFeature.Collision;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _SnakeGame.BlockFeature
{
    public abstract class BaseBlock : SerializedScriptableObject
    {
        [MinValue(0.001f)]
        public float movementMultiplier;
        [Required]
        public Sprite sprite;
        
        public abstract ICollisionResponse CollisionResponse { get; }
    }
}