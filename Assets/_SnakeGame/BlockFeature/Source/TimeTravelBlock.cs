﻿using _SnakeGame.BlockFeature.Collision;
using _SnakeGame.SaveFeature;
using UnityEngine;

namespace _SnakeGame.BlockFeature
{
    [CreateAssetMenu(menuName = "Snake/Block Time Travel")]
    public class TimeTravelBlock : BaseBlock
    {
        private ICollisionResponse _collisionResponse;
        
        public void Initialize(MapSaveState mapSaveState, SaveState saveState)
        {
            _collisionResponse = new TimeTravelCollisionResponse(saveState, mapSaveState);
        }

        public override ICollisionResponse CollisionResponse => _collisionResponse;
    }
}