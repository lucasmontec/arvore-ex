﻿using _SnakeGame.SaveFeature;
using _SnakeGame.SnakeFeature;
using _SnakeGame.SnakeFeature.Parts;

namespace _SnakeGame.BlockFeature.Collision
{
    public class TimeTravelCollisionResponse : ICollisionResponse
    {
        private readonly SaveState _saveState;

        private readonly MapSaveState _mapSaveState;

        public TimeTravelCollisionResponse(SaveState saveState, MapSaveState mapSaveState)
        {
            _saveState = saveState;
            _mapSaveState = mapSaveState;
        }

        public bool HandleCollision(Snake owner)
        {
            _mapSaveState.LoadState(_saveState);
            return true;
        }

        public void SetOwnerPart(SnakePart snakePart) { }
    }
}