﻿using _SnakeGame.SnakeFeature;
using _SnakeGame.SnakeFeature.Parts;
using Sirenix.OdinInspector;

namespace _SnakeGame.BlockFeature.Collision
{
    public abstract class AbstractCollisionResponse : SerializedScriptableObject, ICollisionResponse
    {
        protected SnakePart SnakePart;
        
        /// <summary>
        /// Handles a snake collision.
        /// </summary>
        /// <returns>true cancel the collision.</returns>
        public abstract bool HandleCollision(Snake owner);

        public void SetOwnerPart(SnakePart part)
        {
            SnakePart = part;
        }
    }
}