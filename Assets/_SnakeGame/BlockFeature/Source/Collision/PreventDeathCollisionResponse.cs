﻿using System;
using _SnakeGame.SnakeFeature;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _SnakeGame.BlockFeature.Collision
{
    [CreateAssetMenu(menuName = "Snake/Collision Responses/Prevent Collision Response")]
    public class PreventDeathCollisionResponse : AbstractCollisionResponse
    {
        [NonSerialized]
        private bool _used;

        [Required]
        public Sprite disabledPartSprite;
        
        public override bool HandleCollision(Snake owner)
        {
            if (_used) return false;
            _used = true;

            SnakePart.SetSprite(disabledPartSprite);
            
            return true;
        }
    }
}