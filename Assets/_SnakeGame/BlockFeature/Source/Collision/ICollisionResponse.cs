﻿using _SnakeGame.SnakeFeature;
using _SnakeGame.SnakeFeature.Parts;

namespace _SnakeGame.BlockFeature.Collision
{
    public interface ICollisionResponse
    {
        /// <summary>
        /// Handles a snake collision.
        /// </summary>
        /// <returns></returns>
        bool HandleCollision(Snake owner);

        void SetOwnerPart(SnakePart snakePart);
    }
}