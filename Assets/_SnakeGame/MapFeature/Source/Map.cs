﻿using System.Collections.Generic;
using System.Linq;
using _SnakeGame.BlockFeature.Pickup;
using _SnakeGame.SnakeFeature;
using Sirenix.Utilities;
using Unity.Mathematics;
using UnityEngine;

namespace _SnakeGame.MapFeature
{
    public class Map
    {
        private readonly int2 _size;

        private readonly List<Snake> _snakes = new();

        public int2 Size => _size;
        public int PickupCount => PickupControllers.Count;

        public IEnumerable<Snake> Snakes => _snakes.AsReadOnly();

        public Dictionary<int2, PickupController>PickupControllers { get; } = new();

        public Map(int2 size)
        {
            _size = size;
        }

        public void Clear()
        {
            _snakes.ForEach(s => s.Destroy());
            _snakes.Clear();
            PickupControllers.ForEach(kvp => Object.Destroy(kvp.Value.gameObject));
            PickupControllers.Clear();
        }

        public void AddSnake(Snake snake)
        {
            _snakes.Add(snake);
            snake.OnDeath += () => _snakes.Remove(snake);
        }
        
        public bool IsPositionFree(int2 position)
        {
            IEnumerable<int2> snakePositions = _snakes.SelectMany(snake => snake.PartPositions);
            bool isPositionFree = snakePositions.All(pos => !pos.Equals(position));
            return isPositionFree;
        }

        public bool InBounds(int2 position)
        {
            if (position.x < 0 || position.x > _size.x)
            {
                return false;
            }
            
            return position.y >= 0 && position.y <= _size.y;
        }
        
        public PickupController PeekPickupControllerAt(int2 position)
        {
            return !PickupControllers.ContainsKey(position) ? null : PickupControllers[position];
        }
        
        public PickupController GetPickupControllerAt(int2 position)
        {
            if (!PickupControllers.ContainsKey(position)) 
                return null;
            
            var pickup = PickupControllers[position];
            PickupControllers.Remove(position);
            return pickup;
        }

        public void AddPickup(int2 position, PickupController pickup)
        {
            PickupControllers[position] = pickup;
        }

        public void RemovePickup(int2 position)
        {
            PickupControllers.Remove(position);
        }

        public float TotalDistanceToPickups(int2 position)
        {
            return PickupControllers.Keys.Sum(pickupPosition => math.distancesq(position, pickupPosition));
        }
    }
}