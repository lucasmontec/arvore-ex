using _SnakeGame.Util;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;

namespace _SnakeGame.MapFeature
{
    public class MapController : MonoBehaviour
    {
        [SerializeField, OnValueChanged(nameof(UpdateBorderSprite))]
        private int2 mapSize;

        [SerializeField, Required]
        private SpriteRenderer borderSprite;

        public Map Map { get; private set; }

        public int2 MapSize => mapSize;
        
        public void Awake()
        {
            Map = new Map(mapSize);
            transform.position += mapSize.ToVector3() * 0.5f;
        }

        private void UpdateBorderSprite()
        {
            borderSprite.size = new Vector2(mapSize.x + 2, mapSize.y + 2);
        }
    }
}
