﻿using _SnakeGame.BlockFeature;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _SnakeGame.SnakeFeature
{
    [CreateAssetMenu(menuName = "Snake/Snake Initial Type")]
    public class SnakeType : SerializedScriptableObject
    {
        [SerializeField]
        private Block[] blocks;

        public Block[] Blocks => blocks;
    }
}