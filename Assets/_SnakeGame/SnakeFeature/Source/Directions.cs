﻿using System.Collections.Generic;
using Unity.Mathematics;

namespace _SnakeGame.SnakeFeature
{
    public static class Directions
    {
        public static readonly int2 Left = new int2(-1, 0);
        public static readonly int2 Right = new int2(1, 0);
        public static readonly int2 Up = new int2(0, 1);
        public static readonly int2 Down = new int2(0, -1);

        public static readonly List<int2> All = new() {Left, Right, Up, Down};
    }
}