﻿using System;
using _SnakeGame.SnakeFeature.InputControllers;
using Sirenix.OdinInspector;
using UnityEngine.Serialization;

namespace _SnakeGame.SnakeFeature
{
    [Serializable]
    public class SnakeConfiguration
    {
        public SnakeController controller = SnakeController.Human;

        [ShowIf(nameof(controller), SnakeController.AI)]
        public SnakeAIInputController.AIDifficulty aiDifficulty;

        [Required]
        public SnakeType snakeType;

        [FormerlySerializedAs("Input")] 
        [ShowIf(nameof(controller), SnakeController.Human)]
        public PlayerInputKeys input;

        public SnakeConfiguration Clone()
        {
            return new SnakeConfiguration
            {
                controller = controller,
                aiDifficulty = aiDifficulty,
                snakeType = snakeType,
                input = new PlayerInputKeys(input.rotateLeftKey, input.rotateRightKey)
            };
        }
    }
}