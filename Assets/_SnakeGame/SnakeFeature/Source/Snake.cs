﻿using System;
using System.Collections.Generic;
using System.Linq;
using _SnakeGame.BlockFeature;
using _SnakeGame.BlockFeature.Pickup;
using _SnakeGame.Framework;
using _SnakeGame.MapFeature;
using _SnakeGame.SnakeFeature.Parts;
using Unity.Mathematics;

namespace _SnakeGame.SnakeFeature
{
    public class Snake : BaseTickable
    {
        private readonly List<ISnakePart> _snakeParts = new();

        private readonly float _baseSpeed;
        private readonly ISnakePartFactory _snakePartFactory;
        private readonly SnakePart _partPrefab;
        private readonly Map _map;

        private int2 _nextMovement;
        private int2 _position;

        public event Action OnDeath;
        public event Action OnDestroy;
        public int2 Position => _position;
        public int2 Movement { get; private set; }

        public List<int2> PartPositions => _snakeParts.Select(p => p.Position).ToList();
        public List<BaseBlock> PartBlocks => _snakeParts.Select(p => p.Block).ToList();
        public SnakeConfiguration Configuration { get; }

        private readonly int2[] _initializeTentativeDirections = {
            new(1,0),
            new(-1,0),
            new(0,1),
            new(0,-1)
        };
        
        public Snake(float baseSpeed, ISnakePartFactory snakePartFactory, Map map, SnakeConfiguration configuration) : base(SpeedToInterval(baseSpeed))
        {
            _baseSpeed = baseSpeed;
            _snakePartFactory = snakePartFactory;
            _map = map;
            Configuration = configuration;
        }

        private static float SpeedToInterval(float speed)
        {
            return 1 / speed;
        }
        
        public bool TryInitialize(int2 position, params Block[] blocks)
        {
            if (!_map.InBounds(position)) throw new Exception($"Position {position} outside of the map bounds!");

            foreach (int2 tentativeDirection in _initializeTentativeDirections)
            {
                List<int2> availablePositions = GetAvailableBlockPositionsInDirection(position, blocks.Length, tentativeDirection);
                if (availablePositions.Count != blocks.Length) continue;
                
                SpawnSnake(availablePositions, blocks);
                return true;
            }

            return false;
        }

        public void SpawnSnake(IReadOnlyList<int2> availablePositions, IReadOnlyList<BaseBlock> blocks)
        {
            for (int blockId = 0; blockId < blocks.Count; blockId++)
            {
                AddPart(availablePositions[blockId], blocks[blockId]);
            }

            _position = availablePositions[0];
            Movement = availablePositions[0] - availablePositions[1];
            _nextMovement = Movement;
        }

        private List<int2> GetAvailableBlockPositionsInDirection(int2 startingPosition, int blockCount, int2 direction)
        {
            List<int2> positions = new();
            for (int blockId = 0; blockId < blockCount; blockId++)
            {
                int2 blockPosition = startingPosition + direction * blockId;
                if (_map.InBounds(blockPosition) && _map.IsPositionFree(blockPosition))
                {
                    positions.Add(blockPosition);
                }
            }
            return positions;
        }

        private void Die()
        {
            OnDeath?.Invoke();
            Destroy();
        }

        public void Destroy()
        {
            OnDestroy?.Invoke();
            foreach (ISnakePart snakePart in _snakeParts)
            {
                snakePart.Destroy();
            }
        }

        public void AddPart(BaseBlock block)
        {
            AddPart(_position, block, false);
        }

        private void AddPart(int2 position, BaseBlock block, bool inTheEnd=true)
        {
            var newPart = _snakePartFactory.Create(position, Movement, block);
            if (inTheEnd)
            {
                _snakeParts.Add(newPart);
            }
            else
            {
                _snakeParts.Insert(0, newPart);
            }
            
            UpdateSnakeSpeed();
        }

        private void UpdateSnakeSpeed()
        {
            float finalSpeed = _snakeParts.Select(part => part.MovementMultiplier)
                .Aggregate(_baseSpeed, (current, multiplier) => current * multiplier);
            float newTickInterval = SpeedToInterval(finalSpeed);
            ChangeTickInterval(newTickInterval);
        }

        protected override void Tick()
        {
            Move();
        }

        private void Move()
        {
            Movement = _nextMovement;

            int2 nextPosition = _position + Movement;
            HandlePrePickup(nextPosition);
            
            _position = nextPosition;

            ClampPosition();

            var hasOtherSnake = !_map.IsPositionFree(_position);
            if (hasOtherSnake)
            {
                var preventCollision = _snakeParts.Any(part => part.CollisionResponse?.HandleCollision(this) ?? false);
                if (!preventCollision)
                {
                    Die();
                }
            }
            
            var hasPickup = HandlePickup();
            if (!hasPickup)
            {
                MoveParts();
            }
        }

        private void HandlePrePickup(int2 nextPosition)
        {
            var pickupController = _map.PeekPickupControllerAt(nextPosition);
            bool hasPickup = pickupController != null;
            if (!hasPickup) return;
            if (pickupController.Pickup is IPrePickupHandler prePickupHandler)
            {
                prePickupHandler.WillPickup(this);
            }
        }
        
        private bool HandlePickup()
        {
            var pickupController = _map.GetPickupControllerAt(_position);
            bool hasPickup = pickupController != null;
            if (hasPickup)
            {
                pickupController.DoPickup(this);
            }

            return hasPickup;
        }

        public void ChangeDirection(int2 direction)
        {
            if (IsInvalidDirection(direction)) return;

            _nextMovement = direction;
        }

        public bool IsInvalidDirection(int2 direction)
        {
            int2 movementDelta = direction + Movement;
            return movementDelta.x == 0 && movementDelta.y == 0;
        }

        private void ClampPosition()
        {
            if (_position.x > _map.Size.x)
            {
                _position.x = 0;
            }

            if (_position.x < 0)
            {
                _position.x = _map.Size.x;
            }

            if (_position.y > _map.Size.y)
            {
                _position.y = 0;
            }

            if (_position.y < 0)
            {
                _position.y = _map.Size.y;
            }
        }

        private void MoveParts()
        {
            if (_snakeParts.Count > 1)
            {
                for (int i = _snakeParts.Count-1; i >= 1; i--)
                {
                    var part = _snakeParts[i];
                    var nextPart = _snakeParts[i - 1];
                    part.SetPosition(nextPart.Position, nextPart.Movement);
                }
            }

            _snakeParts[0].SetPosition(_position, Movement);
        }
    }
}