﻿using System;
using _SnakeGame.Framework;
using _SnakeGame.GameFeature;
using _SnakeGame.MapFeature;
using _SnakeGame.SaveFeature;
using _SnakeGame.SnakeFeature.InputControllers;
using _SnakeGame.SnakeFeature.Parts;
using Unity.Mathematics;
using Object = UnityEngine.Object;

namespace _SnakeGame.SnakeFeature
{
    public class SnakeFactory
    {
        private readonly ISnakePartFactory _snakePartFactory;
        private readonly Map _map;
        private readonly GameSettings _gameSettings;
        private readonly IUpdatableManager _updatableManager;
        private readonly Action<SnakeConfiguration> _scheduleCreateSnake;
        private readonly SnakeLineRenderer _lineRendererPrefab;

        public SnakeFactory(
            ISnakePartFactory snakePartFactory, 
            Map map, 
            GameSettings gameSettings, 
            IUpdatableManager updatableManager, 
            Action<SnakeConfiguration> scheduleCreateSnake,
            SnakeLineRenderer lineRendererPrefab)
        {
            _snakePartFactory = snakePartFactory;
            _map = map;
            _gameSettings = gameSettings;
            _updatableManager = updatableManager;
            _scheduleCreateSnake = scheduleCreateSnake;
            _lineRendererPrefab = lineRendererPrefab;
        }

        public void Create(SnakeConfiguration configuration)
        {
            Snake snake = new(_gameSettings.baseSnakeSpeed, _snakePartFactory, _map, configuration);

            var snakeInput = CreateSnakeInput(snake, configuration);
            
            _updatableManager.AddUpdatables(snakeInput, snake);

            CreateSnakeLineRenderer(snake);
            
            snake.OnDeath += () =>
            {
                _scheduleCreateSnake(configuration);
            };

            snake.OnDestroy += () =>
            {
                _updatableManager.RemoveUpdatables(snake, snakeInput);
            };
            
            InitializeSnake(snake, configuration);
            
            _map.AddSnake(snake);
        }
        
        public void CreateFromSave(SnakeSave snakeSave)
        {
            Snake snake = new(_gameSettings.baseSnakeSpeed, _snakePartFactory, _map, snakeSave.SnakeConfiguration);

            var snakeInput = CreateSnakeInput(snake, snakeSave.SnakeConfiguration);
            
            _updatableManager.AddUpdatables(snakeInput, snake);

            CreateSnakeLineRenderer(snake);
            
            snake.OnDeath += () =>
            {
                _scheduleCreateSnake(snakeSave.SnakeConfiguration);
            };
            
            snake.OnDestroy += () =>
            {
                _updatableManager.RemoveUpdatables(snake, snakeInput);
            };
            
            snake.SpawnSnake(snakeSave.SnakePartPositions, snakeSave.SnakeBlocks);
            
            _map.AddSnake(snake);
        }

        private void CreateSnakeLineRenderer(Snake snake)
        {
            var renderer = Object.Instantiate(_lineRendererPrefab);
            renderer.SetSnake(snake);
        }
        
        private IUpdatable CreateSnakeInput(Snake snake, SnakeConfiguration configuration)
        {
            return configuration.controller == SnakeController.AI ? 
                new SnakeAIInputController(snake, _map, configuration.aiDifficulty) :
                new SnakeKeyboardInputController(snake, configuration.input);
        }
        
        private void InitializeSnake(Snake snake, SnakeConfiguration configuration)
        {
            bool initialized;
            int tries = 5000;
            do
            {
                var randomPosInMap = new int2(UnityEngine.Random.Range(0, _map.Size.x), UnityEngine.Random.Range(0, _map.Size.y));
                initialized = snake.TryInitialize(randomPosInMap, configuration.snakeType.Blocks);
                tries--;
            } while (!initialized && tries > 0);

            if (!initialized)
            {
                throw new Exception("Failed to initialize snake!");
            }
        }
    }
}