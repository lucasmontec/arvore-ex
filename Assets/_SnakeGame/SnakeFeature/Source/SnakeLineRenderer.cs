﻿using System.Collections.Generic;
using System.Linq;
using _SnakeGame.Util;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;

namespace _SnakeGame.SnakeFeature
{
    public class SnakeLineRenderer : SerializedMonoBehaviour
    {
        [Required]
        public LineRenderer lineRenderer;

        private Snake _snake;
        
        public void SetSnake(Snake snake)
        {
            _snake = snake;
            _snake.OnDestroy += Destroy;
        }

        private void Destroy()
        {
            Destroy(gameObject);
        }
        
        private void FixedUpdate()
        {
            if(_snake == null) return;
            
            IList<int2> snakePartPositions = SanitizePositionsByDistance(_snake.PartPositions.ToList());
            
            lineRenderer.positionCount = snakePartPositions.Count;
            lineRenderer.SetPositions(snakePartPositions.Select(p => p.ToVector3()).ToArray());
        }

        public static IList<int2> SanitizePositionsByDistance(List<int2> positions)
        {
            IList<int2> sanitized = new List<int2>();
            bool broken = false;
            for (int i = 0; i < positions.Count-1; i++)
            {
                int2 currentPosition = positions[i];
                int2 nextPosition = positions[i+1];

                sanitized.Add(currentPosition);
                //2 > sq(1)
                if (math.distance(currentPosition, nextPosition) <= 2f) continue;
                broken = true;
                break;
            }

            if (!broken)
            {
                sanitized.Add(positions[^1]);
            }

            return sanitized;
        }
    }
}