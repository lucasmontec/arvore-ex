﻿using System.Collections.Generic;
using System.Linq;
using _SnakeGame.Framework;
using _SnakeGame.MapFeature;
using _SnakeGame.Util;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace _SnakeGame.SnakeFeature.InputControllers
{
    public class SnakeAIInputController : IUpdatable
    {
        private readonly Snake _snake;
        private readonly Map _map;
        private readonly AIDifficulty _difficulty;

        private int2 _lastSnakePosition;
        
        public enum AIDifficulty
        {
            Hard,
            Easy
        }

        public SnakeAIInputController(Snake snake, Map map, AIDifficulty difficulty)
        {
            _snake = snake;
            _map = map;
            _difficulty = difficulty;
        }
        
        public void Update()
        {
            if(_lastSnakePosition.Equals(_snake.Position)) return;
            
            var bestDirection = GetNextDirectionBasedOnDifficulty();
            _snake.ChangeDirection(bestDirection);
            _lastSnakePosition = _snake.Position;
        }

        private int2 GetNextDirectionBasedOnDifficulty()
        {
            return _difficulty == AIDifficulty.Easy ? 
                GetNextBestDirectionRandom() : 
                GetNextBestDirection();
        }
        
        private int2 GetNextBestDirection()
        {
            float bestScore = float.NegativeInfinity;
            int2 nextDirection = default;
            foreach (var direction in Directions.All)
            {
                if(_snake.IsInvalidDirection(direction)) continue;

                var score = GetPositionScore(_snake.Position + direction);
                if (score < bestScore) continue;
                
                bestScore = score;
                nextDirection = direction;
            }
            return nextDirection;
        }

        private int2 GetNextBestDirectionRandom()
        {
            RandomWeightedList<int2> nextDirectionList = new RandomWeightedList<int2>(Random.Range, Random.Range);

            foreach (var direction in Directions.All)
            {
                if(_snake.IsInvalidDirection(direction)) continue;

                var score = GetPositionScore(_snake.Position + direction);
                nextDirectionList.Add(direction, score);
            }

            int2 nextDirection = nextDirectionList.GetWeightedRandom();
            return nextDirection;
        }

        private float GetPositionScore(int2 snakePosition)
        {
            IList<int2> snakeParts = _map.Snakes
                .SelectMany(snake => snake.PartPositions).ToList();
            
            var totalDistanceToOtherSnakeParts = snakeParts
                .Select(partPosition => math.distancesq(_snake.Position, partPosition))
                .Sum();

            float snakePartOnTargetPenalty = snakeParts.Any(part => part.Equals(snakePosition)) ? 100f : 0f;
            
            float distanceToPickup = _map.TotalDistanceToPickups(snakePosition);
            return totalDistanceToOtherSnakeParts - distanceToPickup - snakePartOnTargetPenalty;
        }
    }
}