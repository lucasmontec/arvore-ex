﻿using System;
using UnityEngine;

namespace _SnakeGame.SnakeFeature.InputControllers
{
    [Serializable]
    public struct PlayerInputKeys
    {
        public KeyCode rotateLeftKey;
        public KeyCode rotateRightKey;

        public PlayerInputKeys(KeyCode leftKey, KeyCode rightKey)
        {
            rotateLeftKey = leftKey;
            rotateRightKey = rightKey;
        }
    }
}