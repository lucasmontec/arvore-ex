﻿using _SnakeGame.Framework;
using _SnakeGame.Util;
using UnityEngine;

namespace _SnakeGame.SnakeFeature.InputControllers
{
    public class SnakeKeyboardInputController : IUpdatable
    {
        private readonly Snake _snake;
        private readonly PlayerInputKeys _inputKeys;

        public SnakeKeyboardInputController(Snake snake, PlayerInputKeys inputKeys)
        {
            _snake = snake;
            _inputKeys = inputKeys;
        }

        public void Update()
        {
            HandleInput();
        }

        private void HandleInput()
        {
            if (Input.GetKeyDown(_inputKeys.rotateLeftKey))
            {
                var nextDirection = _snake.Movement.RotateLeft();
                _snake.ChangeDirection(nextDirection);
            }
            
            if (Input.GetKeyDown(_inputKeys.rotateRightKey))
            {
                var nextDirection = _snake.Movement.RotateRight();
                _snake.ChangeDirection(nextDirection);
            }
        }
    }
}