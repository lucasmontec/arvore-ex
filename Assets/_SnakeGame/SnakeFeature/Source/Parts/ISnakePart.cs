﻿using _SnakeGame.BlockFeature;
using _SnakeGame.BlockFeature.Collision;
using Unity.Mathematics;

namespace _SnakeGame.SnakeFeature.Parts
{
    public interface ISnakePart
    {
        BaseBlock Block { get; }
        float MovementMultiplier { get; }
        int2 Position { get; }
        int2 Movement { get; }
        ICollisionResponse CollisionResponse { get; }
        void Setup(BaseBlock block);
        void SetPosition(int2 position, int2 movement);
        void Destroy();
    }
}