﻿using _SnakeGame.BlockFeature;
using _SnakeGame.BlockFeature.Collision;
using _SnakeGame.Util;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;

namespace _SnakeGame.SnakeFeature.Parts
{
    public class SnakePart : SerializedMonoBehaviour, ISnakePart
    {
        public BaseBlock Block { get; private set; }
        public float MovementMultiplier { get; private set; }

        [FormerlySerializedAs("partImage")] [Required]
        public SpriteRenderer spriteRenderer;

        public int2 Position { get; private set; }
        public int2 Movement { get; private set; }

        public ICollisionResponse CollisionResponse { get; private set; }
        
        public void Setup(BaseBlock block)
        {
            Block = block;
            MovementMultiplier = block.movementMultiplier;
            spriteRenderer.sprite = block.sprite;
            SetupCollisionResponse(block);
        }

        private void SetupCollisionResponse(BaseBlock block)
        {
            CollisionResponse = block.CollisionResponse;
            CollisionResponse?.SetOwnerPart(this);
        }

        public void SetPosition(int2 position, int2 movement)
        {
            Movement = movement;
            UpdateRotation();

            Position = position;
            transform.position = position.ToVector2();
        }

        private void UpdateRotation()
        {
            transform.rotation = GetMovementRotation();
            spriteRenderer.flipX = Movement.x > 0;
        }

        private Quaternion GetMovementRotation()
        {
            return Movement.y switch
            {
                < 0 => Quaternion.Euler(0, 0, 90),
                > 0 => Quaternion.Euler(0, 0, -90),
                _ => Quaternion.Euler(0, 0, 0)
            };
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void SetSprite(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;
        }
    }
}