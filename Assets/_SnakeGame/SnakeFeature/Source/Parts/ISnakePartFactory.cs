﻿using _SnakeGame.BlockFeature;
using Unity.Mathematics;

namespace _SnakeGame.SnakeFeature.Parts
{
    public interface ISnakePartFactory
    {
        public ISnakePart Create(int2 position, int2 movement, BaseBlock block);
    }
}