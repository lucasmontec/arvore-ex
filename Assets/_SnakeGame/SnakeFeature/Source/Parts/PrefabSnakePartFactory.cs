﻿using _SnakeGame.BlockFeature;
using Unity.Mathematics;
using UnityEngine;

namespace _SnakeGame.SnakeFeature.Parts
{
    public class PrefabSnakePartFactory : ISnakePartFactory
    {
        private readonly SnakePart _partPrefab;
        private readonly Transform _root;

        public PrefabSnakePartFactory(SnakePart partPrefab, Transform root)
        {
            _partPrefab = partPrefab;
            _root = root;
        }

        public ISnakePart Create(int2 position, int2 movement, BaseBlock block)
        {
            SnakePart snakePart = Object.Instantiate(_partPrefab, Vector3.zero, Quaternion.identity);
            snakePart.transform.SetParent(_root, false);
            snakePart.SetPosition(position, movement);
            snakePart.Setup(block);
            return snakePart;
        }
    }
}